import { Component } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";

@Component({
  selector: "app-mieasy",
  templateUrl: "./mieasy.component.html",
  styleUrls: ["./mieasy.component.scss"]
})
export class MieasyComponent {
  slidesStore = [
    { id: "1", src: "/assets/images/slide-01.png", alt: "img1", title: "one" },
    { id: "2", src: "/assets/images/slide-02.png", alt: "img2", title: "two" },
    { id: "3", src: "/assets/images/slide-03.png", alt: "img3", title: "three" }
  ];

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    items: 1,
    dots: true,
    nav: false
  };

  constructor() {}
}
