import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MieasyComponent } from './mieasy.component';

describe('MieasyComponent', () => {
  let component: MieasyComponent;
  let fixture: ComponentFixture<MieasyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MieasyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MieasyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
