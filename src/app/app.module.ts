import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NaviComponent } from "./common/navi/navi.component";
import { HeaderComponent } from "./common/header/header.component";
import { TrustedbyComponent } from "./common/trustedby/trustedby.component";
import { MieasyComponent } from "./common/mieasy/mieasy.component";
import { CarouselModule } from "ngx-owl-carousel-o";
import { ArtworkComponent } from "./common/artwork/artwork.component";
import { UploadComponent } from "./common/upload/upload.component";

@NgModule({
  declarations: [
    AppComponent,
    NaviComponent,
    HeaderComponent,
    TrustedbyComponent,
    MieasyComponent,
    ArtworkComponent,
    UploadComponent
  ],
  imports: [BrowserModule, AppRoutingModule, CarouselModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
export class SomeModule {}
